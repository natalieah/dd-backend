from django import forms

from .models import Quiz


class QuizForm(forms.ModelForm):

    class Meta:

        model = Quiz
        fields = (
            'published_date',
            'title',
            'description',
            'sponsor',
            'price',
            'player1',
            'player2',
            'player1_img',
            'player2_img',
            'player1_score',
            'player2_score',
            'first_category',
            'first_question',
            'first_answer',
            'first_hint5',
            'first_hint4',
            'first_hint3',
            'first_hint2',
            'first_hint1',
            'second_category',
            'second_question',
            'second_answer',
            'second_hint5',
            'second_hint4',
            'second_hint3',
            'second_hint2',
            'second_hint1',
            'third_category',
            'third_question',
            'third_answer',
            'third_hint5',
            'third_hint4',
            'third_hint3',
            'third_hint2',
            'third_hint1',
            'fourth_category',
            'fourth_question',
            'fourth_answer',
            'fourth_hint5',
            'fourth_hint4',
            'fourth_hint3',
            'fourth_hint2',
            'fourth_hint1',
            'fifth_category',
            'fifth_question',
            'fifth_answer',
            'fifth_hint5',
            'fifth_hint4',
            'fifth_hint3',
            'fifth_hint2',
            'fifth_hint1',
            'is_draft'
        )
