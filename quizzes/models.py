# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils import timezone

# Create your models here.


class Quiz(models.Model):

    # Quiz Intro
    title = models.CharField(
        'Titel', max_length=200, default='', help_text="Titeln för veckans Duell", blank=True
    )
    description = models.TextField('Quiz intro', default='', help_text="Kort intro text", blank=True)
    sponsor = models.CharField(
        'Sponsor', max_length=200, default='', blank=True, help_text="Sponsoren för veckans duell"
    )
    price = models.CharField(
        'Pris', max_length=200, default='', blank=True, help_text="Veckans pris"
    )

    #Players
    player1_img = models.ImageField(upload_to='media/', default='', blank=True)
    player2_img = models.ImageField(upload_to='media/', default='', blank=True)
    player1 = models.CharField('Spelare', max_length=70, default='', help_text="Namn på spelare 1", blank=True)
    player1_score = models.IntegerField(
        'Poäng', max_length=None, null=True, help_text='Poäng för spelare 1', blank=True
    )
    player2 = models.CharField('Spelare', max_length=70, default='', help_text="Namn på spelare 2", blank=True)
    player2_score = models.IntegerField(
        'Poäng', max_length=None, null=True, help_text='Poäng för spelare 2', blank=True
    )

    # First Question
    first_category = models.CharField(
        'Kategori', max_length=200, default='', help_text="Kategori, tex. Mat & dryck.", blank=True
    )
    first_question = models.CharField(
        'Fråga', max_length=200, default='', help_text="Fråga, det vi söker efter. Tex. En frukt.", blank=True
    )
    first_answer = models.CharField(
        'Svar', max_length=200, default='', help_text="Svaret på frågan.", blank=True
    )
    first_hint5 = models.TextField('Ledtråd', default='', help_text="5 poängs ledtråd", blank=True)
    first_hint4 = models.TextField('Ledtråd', default='', help_text="4 poängs ledtråd", blank=True)
    first_hint3 = models.TextField('Ledtråd', default='', help_text="3 poängs ledtråd", blank=True)
    first_hint2 = models.TextField('Ledtråd', default='', help_text="2 poängs ledtråd", blank=True)
    first_hint1 = models.TextField('Ledtråd', default='', help_text="1 poängs ledtråd", blank=True)

    # Second Question
    second_category = models.CharField(
        'Kategori', max_length=200, default='', help_text="Kategori, tex. Mat & dryck.", blank=True
    )
    second_question = models.CharField(
        'Fråga', max_length=200, default='', help_text="Fråga, det vi söker efter. Tex. En frukt.", blank=True
    )
    second_answer = models.CharField(
        'Svar', max_length=200, default='', help_text="Svaret på frågan.", blank=True
    )
    second_hint5 = models.TextField(
        'Ledtråd', max_length=200, default='', help_text="5 poängs ledtråd", blank=True
    )
    second_hint4 = models.TextField('Ledtråd', default='', help_text="4 poängs ledtråd", blank=True)
    second_hint3 = models.TextField('Ledtråd', default='', help_text="3 poängs ledtråd", blank=True)
    second_hint2 = models.TextField('Ledtråd', default='', help_text="2 poängs ledtråd", blank=True)
    second_hint1 = models.TextField('Ledtråd', default='', help_text="1 poängs ledtråd", blank=True)

    # Third Question
    third_category = models.CharField(
        'Kategori', max_length=200, default='', help_text="Kategori, tex. Mat & dryck.", blank=True
    )
    third_question = models.CharField(
        'Fråga', max_length=200, default='', help_text="Fråga, det vi söker efter. Tex. En frukt.", blank=True
    )
    third_answer = models.CharField(
        'Svar', max_length=200, default='', help_text="Svaret på frågan.", blank=True
    )
    third_hint5 = models.TextField(
        'Ledtråd', max_length=200, default='', help_text="5 poängs ledtråd", blank=True
    )
    third_hint4 = models.TextField('Ledtråd', default='', help_text="4 poängs ledtråd", blank=True)
    third_hint3 = models.TextField('Ledtråd', default='', help_text="3 poängs ledtråd", blank=True)
    third_hint2 = models.TextField('Ledtråd', default='', help_text="2 poängs ledtråd", blank=True)
    third_hint1 = models.TextField('Ledtråd', default='', help_text="1 poängs ledtråd", blank=True)

    # Fourth Question
    fourth_category = models.CharField(
        'Kategori', max_length=200, default='', help_text="Kategori, tex. Mat & dryck.", blank=True
    )
    fourth_question = models.CharField(
        'Fråga', max_length=200, default='', help_text="Fråga, det vi söker efter. Tex. En frukt.", blank=True
    )
    fourth_answer = models.CharField(
        'Svar', max_length=200, default='', help_text="Svaret på frågan.", blank=True
    )
    fourth_hint5 = models.TextField(
        'Ledtråd', max_length=200, default='', help_text="5 poängs ledtråd", blank=True
    )
    fourth_hint4 = models.TextField('Ledtråd', default='', help_text="4 poängs ledtråd", blank=True)
    fourth_hint3 = models.TextField('Ledtråd', default='', help_text="3 poängs ledtråd", blank=True)
    fourth_hint2 = models.TextField('Ledtråd', default='', help_text="2 poängs ledtråd", blank=True)
    fourth_hint1 = models.TextField('Ledtråd', default='', help_text="1 poängs ledtråd", blank=True)

    # Fifth Question
    fifth_category = models.CharField(
        'Kategori', max_length=200, default='', help_text="Kategori, tex. Mat & dryck.", blank=True
    )
    fifth_question = models.CharField(
        'Fråga', max_length=200, default='', help_text="Fråga, det vi söker efter. Tex. En frukt.", blank=True
    )
    fifth_answer = models.CharField(
        'Svar', max_length=200, default='', help_text="Svaret på frågan.", blank=True
    )
    fifth_hint5 = models.TextField(
        'Ledtråd', max_length=200, default='', help_text="5 poängs ledtråd", blank=True
    )
    fifth_hint4 = models.TextField('Ledtråd', default='', help_text="4 poängs ledtråd", blank=True)
    fifth_hint3 = models.TextField('Ledtråd', default='', help_text="3 poängs ledtråd", blank=True)
    fifth_hint2 = models.TextField('Ledtråd', default='', help_text="2 poängs ledtråd", blank=True)
    fifth_hint1 = models.TextField('Ledtråd', default='', help_text="1 poängs ledtråd", blank=True)

    published_date = models.DateTimeField(blank=True, null=True)
    created_date = models.DateTimeField(default=timezone.now, blank=True)
    is_draft = models.BooleanField(default=True)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title
