from __future__ import unicode_literals

from django.utils import timezone
from django.shortcuts import render, redirect, get_object_or_404
from django.http import Http404
from django.template import loader
from django.core.mail import send_mail, BadHeaderError
from django.template.loader import render_to_string
from rest_framework import status, generics
from rest_framework.views import APIView
from rest_framework.response import Response

from .models import Quiz
from .serializers import QuizSerializer
from .forms import QuizForm


# Create your views here.
class ListQuiz(APIView):

    def get(self, request, format=None):
        quizzes = Quiz.objects.filter(is_draft=False)
        serializer = QuizSerializer(quizzes, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = QuizSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class DetailQuiz(APIView):

    def get_object(self, pk):
        try:
            return Quiz.objects.get(pk=pk)
        except Quiz.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        quiz = self.get_object(pk)
        serializer = QuizSerializer(quiz)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = QuizSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


def post_new(request):
    if request.method == "POST":
        form = QuizForm(request.POST, request.FILES)
        if form.is_valid():
            quiz = form.save(commit=False)
            quiz.author = request.user
            quiz.save()
        return redirect('post_detail', pk=quiz.pk)
    else:
        form = QuizForm()
    return render(request, 'quizzes/post_new.html', {'form': form})


def post_list(request):
    posts = Quiz.objects.all().order_by('-created_date')
    return render(request, 'quizzes/post_list.html', {'posts': posts})


def post_detail(request, pk):
    post = get_object_or_404(Quiz, pk=pk)
    return render(request, 'quizzes/post_detail.html', {'post': post})


def post_edit(request, pk):
    post = get_object_or_404(Quiz, pk=pk)
    if request.method == "POST":
        form = QuizForm(request.POST, instance=post)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.save()
            return redirect('post_detail', pk=post.pk)
    else:
        form = QuizForm(instance=post)
    return render(request, 'quizzes/post_edit.html', {'form': form})

def post_draft_list(request):
    posts = Quiz.objects.filter(is_draft=True).order_by('-created_date')
    return render(request, 'quizzes/post_draft_list.html', {'posts': posts})

def post_publish(request, pk):
    post = get_object_or_404(Quiz, pk=pk)
    post.is_draft ^= True
    post.publish()
    mail_plain = render_to_string(
        'quizzes/post_email.txt', {
        'title': post.title,
        'description': post.description,
        'player1': post.player1,
        'player2': post.player2,
        'player1_score': post.player1_score,
        'player2_score': post.player2_score,
        'first_question': post.first_question,
        'first_category': post.first_category,
        'first_hint1': post.first_hint1,
        'first_hint2': post.first_hint2,
        'first_hint3': post.first_hint3,
        'first_hint4': post.first_hint4,
        'first_hint5': post.first_hint5,
        'first_answer': post.first_answer,
        'second_question': post.second_question,
        'second_category': post.second_category,
        'second_hint1': post.second_hint1,
        'second_hint2': post.second_hint2,
        'second_hint3': post.second_hint3,
        'second_hint4': post.second_hint4,
        'second_hint5': post.second_hint5,
        'second_answer': post.second_answer,
        'second_question': post.second_question,
        'third_category': post.third_category,
        'third_hint1': post.third_hint1,
        'third_hint2': post.third_hint2,
        'third_hint3': post.third_hint3,
        'third_hint4': post.third_hint4,
        'third_hint5': post.third_hint5,
        'third_answer': post.third_answer,
        'fourth_question': post.fourth_question,
        'fourth_category': post.fourth_category,
        'fourth_hint1': post.fourth_hint1,
        'fourth_hint2': post.fourth_hint2,
        'fourth_hint3': post.fourth_hint3,
        'fourth_hint4': post.fourth_hint4,
        'fourth_hint5': post.fourth_hint5,
        'fourth_answer': post.fourth_answer,
        'fifth_question': post.fifth_question,
        'fifth_category': post.fifth_category,
        'fifth_hint1': post.fifth_hint1,
        'fifth_hint2': post.fifth_hint2,
        'fifth_hint3': post.fifth_hint3,
        'fifth_hint4': post.fifth_hint4,
        'fifth_hint5': post.fifth_hint5,
        'fifth_answer': post.fifth_answer,
        })
    try:
        send_mail(
            'Duellen',
            mail_plain,
            'some@sender.com',
            ['some@reciever.com'],
        )
    except BadHeaderError:
        return HttpResponse('Invalid header found.')
    return redirect('post_detail', pk=pk)

def post_remove(request, pk):
    post = get_object_or_404(Quiz, pk=pk)
    post.delete()
    return redirect('post_list')
